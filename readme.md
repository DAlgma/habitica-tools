# Habitica Party Tools

Some Tools to help Habitica parties decide which Quest they should do next.

### Original Repo

This tool was originally created by pietervanh in [this repo](https://bitbucket.org/pietervanh/habitica-tools/src/master/).
With his permission I took over this project in October 2021 to improve it further.

### Prerequisites

NodeJs / npm


### Installing


> npm install
> npm start


## Deployment

I use the awesome [surge.sh](http://surge.sh)

## Built With

* [Preact](https://preactjs.com/) - The web framework used
* [Mobx](https://mobx.js.org/) - State Management
* [Semantic UI](https://semantic-ui.com) - Styles

## Contributing

Feel free to create forks and pull requests or contact me in Habitica @EstGoddess or at dianx93@gmail.com.


## Authors

Original Author [PieterV](https://bitbucket.org/pietervanh/)

Current maintainer [DAlgma](https://bitbucket.org/dalgma/)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Habitica for making my life more organized 
